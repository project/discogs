* Issue #2052903 by Karlheinz: Fixed Import release process broken after first
  page
* Issue #2052555 by Karlheinz: Implemented an additional (optional) field in the
  array returned from hooks
* Issue #2043187 by Karlheinz: Fixed Undefined index: main_release in
  _discogs_prov_common() line 406
* Issue #2035205 by Karlheinz: Fixed AJAX error on import
* Issue #2035247 by Karlheinz: On Track Field creation/preview, warning
  generated if user didn't provide any track information
2013-07-02: 7.x-1.0-beta3 tag
* Fixed bug where editing Taxonomy terms caused catastrophic PDO failures
* Fixed formatting issues with imported Releases containing HTML markup in
  Credits and Notes fields
* Fixed some formatting issues with the Track Field template
* Removed the (useless) Track Field page
2013-07-01: 7.x-1.0-beta2 tag
* Removed debugging info from 7.x-1.0-beta1 (oops)
2013-07-01: 7.x-1.0-beta1 tag
* Search filters rewritten: now specified per search type
* Fixed some permissions bugs
* Added .api.php hook documentation in discog_mediator/docs
* Implemented hook_help() for all modules
* Created README.txt
* Various minor code fixes
2013-06-29: 7.x-1.0-alpha2 tag. Added features:
* Providers have ability to specify search filters (though Discogs.com doesn't)
* Import now uses the Batch API
* Implemented an "accidental feature" from the D6 version: if the term is
  numeric, assume that it is a release ID
* Various bug fixes in multistep form process
2013-06-28: 7.x-1.0-alpha tag
MULTIPLE revisions to 7.x-1.x branch prior to alpha release.
Issue #1210512 by Karlheinz: Started Drupal 7.x-1.x version.