<?php
/**
 * @file
 * Administration page callbacks for the Discogs.com Provider module.
 */

/**
 * Form builder for Discogs.com Provider admin settings.
 */
function discogs_prov_admin_settings() {
  $info_str = t('For more information, see <a href="!url">Discogs.com Authentication</a>.',
    array('!url' => 'http://www.discogs.com/developers/#page:authentication'));
  $form['discogs_prov_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Discogs.com Consumer Key'),
    '#default_value' => variable_get('discogs_prov_key'),
    '#description' => t('Discogs.com Consumer Key for your application.')
      . ' ' . $info_str,
    '#size' => 20,
    '#maxlength' => 30,
  );
  $form['discogs_prov_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Discogs.com Consumer Secret'),
    '#default_value' => variable_get('discogs_prov_secret'),
    '#description' => t('Discogs.com Consumer Secret for this website.')
      . ' ' . $info_str,
    '#size' => 32,
    '#maxlength' => 40,
  );
  return system_settings_form($form);
}

